package com.mzhurov.people.rest;

public class Response {
    private int peopleCount;

    public int getPeopleCount() {
        return peopleCount;
    }

    public void setPeopleCount(final int peopleCount) {
        this.peopleCount = peopleCount;
    }
}
