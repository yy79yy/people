package com.mzhurov.people.rest;

import com.mzhurov.people.bl.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PeopleController {

    private final PeopleService peopleService;

    @Autowired
    public PeopleController(final PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @PostMapping(value = "/people", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> postPeople(@RequestBody final int year) {
        final int peopleCount = peopleService.countPeopleWithSpecificBirthYear(year);

        final Response response = new Response();
        response.setPeopleCount(peopleCount);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
