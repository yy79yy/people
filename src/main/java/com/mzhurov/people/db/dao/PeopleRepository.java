package com.mzhurov.people.db.dao;

import com.mzhurov.people.db.entity.PeopleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PeopleRepository extends JpaRepository<PeopleEntity, String> {

    @Query("select count(p) from PeopleEntity p where year(p.dateOfBirth) = :year")
    int countPeopleWithSpecificBirthYear(@Param(("year")) final int year);
}
