package com.mzhurov.people.bl;

public interface PeopleService {
    int countPeopleWithSpecificBirthYear(final int year);
}
