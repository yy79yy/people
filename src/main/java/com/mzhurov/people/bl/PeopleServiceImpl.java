package com.mzhurov.people.bl;

import com.mzhurov.people.db.dao.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeopleServiceImpl implements PeopleService {

    private final PeopleRepository peopleRepository;

    @Autowired
    public PeopleServiceImpl(final PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public int countPeopleWithSpecificBirthYear(final int year) {
        return peopleRepository.countPeopleWithSpecificBirthYear(year);
    }
}
